using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartMenu : MonoBehaviour
{
    [SerializeField] private Text _text;
    [SerializeField] private InputField _inputField;
    [SerializeField] private Button _button;

    private void Start()
    {
        UpdateBestScore();
    }

    public void StartGame()
    {
        GameManager.Instance.actualName = _inputField.text;
        SceneManager.LoadScene("main");
    }

    public void UpdateBestScore()
    {
        _text.text =$"Best Score : {GameManager.Instance.nameBestScore} : {GameManager.Instance.bestScore}";

    }
    
}
