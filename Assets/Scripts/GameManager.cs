using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Serialization;

[Serializable]
public class SaveData
{
    public string nameUser;
    public int score;
}
public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public string nameBestScore;
    public string actualName;
    public int actualScore;
    public int bestScore;
    private void Awake()
    {
        // start of new code
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        // end of new code

        Instance = this;
        DontDestroyOnLoad(gameObject);
        LoadBestScore(); 
    }

    public void SaveScore()
    {
        SaveData data = new SaveData();
        data.nameUser = actualName;
        data.score = actualScore;

        string json = JsonUtility.ToJson(data);
  
        File.WriteAllText(Application.persistentDataPath + "/savefile.json", json);
    }
    public void LoadBestScore()
    {
        string path = Application.persistentDataPath + "/savefile.json";
        if (File.Exists(path))
        {
            string json = File.ReadAllText(path);
            SaveData data = JsonUtility.FromJson<SaveData>(json);

            nameBestScore = data.nameUser;
            bestScore = data.score;
        }
    }

    public void ComparePoints(int points)
    {
        if (points > bestScore)
        {
            actualScore = points;
            SaveScore();
            LoadBestScore();
            FindObjectOfType<MainManager>().UpdateUIBestScore();
        }
    }
}
